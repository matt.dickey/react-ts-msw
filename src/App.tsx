import { useCallback, useState } from 'react';
import './App.css';
import { fetchJson } from './fetchJson';

interface Post {
  id: number;
  title: string;
  body: string;
  userId: number;
}

function App() {
  const [posts, setPosts] = useState<Post[]>([]);

  const onFetchPostsClick = useCallback(() => {
    fetchJson<Post[]>('https://jsonplaceholder.typicode.com/posts').then((posts) => {
      setPosts(posts);
    });
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <h1>React + Typescript + MSW</h1>
        <code>Change the .env file and restart the dev server to toggle mocking on/off</code>
        <br />
        <button onClick={onFetchPostsClick}>Fetch data</button>

        {posts?.length ? (
          <ul>
            {posts.map((post) => (
              <li key={post.id}>
                <span>{post.title}</span>
              </li>
            ))}
          </ul>
        ) : null}
      </header>
    </div>
  );
}

export default App;
