import { rest } from 'msw';

const handlers = [
  rest.get('https://jsonplaceholder.typicode.com/posts', async (_req, res, ctx) => {
    return res(ctx.json((await import('./fixtures/posts.json')).default));
  }),
];

export default handlers;
