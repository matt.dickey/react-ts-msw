/// <reference types="react-scripts" />

// define types for environment variables
declare namespace NodeJS {
  interface ProcessEnv {
    REACT_APP_MOCKING_ENABLED: 'true' | 'false';
  }
}
